/*
   Request handlers
*/

#include <Arduino.h>
#include "SPIFFS.h"
#include "handlers.h"
#include "configuration.h"
#include "printf.h"
#include "version.h"

#define PWM_DISABLED 0
#define PWM_ENABLED 1

// conversion from micro-seconds to PWM duty cycle value
float us_to_pwm = 4096 * PWM_FREQ / 1000000.;

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

WebServer *server;

/* 
   Servo states
*/

int servo_states[MAX_SERVOS];

// - pulse generation
int pwm_enabled = 0;

/*
   Internal helpers
*/

void set_pwm_state(int enabled) {
    if (enabled) {
        pwm.wakeup();
    } else {
        pwm.sleep();
    }
    pwm_enabled = enabled;
}

/*
   Send a PWM setting command to the PCA.

    Parameters :
    - servo_id : the servo id (same as the PCA pulse output port)
    - pulse : the pulse widthy (in micro-seconds)
*/
void set_pwm(int servo_id, int pulse) {
    pwm.setPWM(servo_id, 0, int(pulse * us_to_pwm));
    set_pwm_state(PWM_ENABLED);
}

/* 
   Get the room number argument from the route path.

   If not valid, send a HTTP 400 error response directly.

   Returns the room number.
*/
int get_room_arg() {
    String room = server->pathArg(0);
    if (room != "1" && room != "2") {
        server->send(400, "application/json", "{\"detail\": \"invalid room number\"}");
        return 0;
    }
    return room.toInt();
}

/* 
   Get the servo port argument from the route path.

   If not valid, send a HTTP 400 error response directly.

   Returns the port number or -1 if invalid value.
*/
int get_servo_id_arg() {
    String servo_id = server->pathArg(0);
    if (String("01234567").indexOf(servo_id) == -1) {
        server->send(400, "application/json", "{\"detail\": \"invalid port number\"}");
        return -1;
    }
    return servo_id.toInt();
}

/*
   Get an action from the eponym request data.

   If missing, send a HTTP 400 error response directly.

   Returns the action.
*/
String get_action_arg() {
    String action = server->arg("action");
    if (action == "") {
        server->send(400, "application/json", "{\"detail\": \"missing action argument\"}");
        return "";
    }
    return action;
}

#define CONTROL_ACTION      1
#define CONTROL_POSITION    2
#define CONTROL_PULSE       3
#define CONTROL_PERCENT     4

typedef struct {
    uint8_t parm_type;
    union {
        char str_value[10];
        int int_value;
    };
} ControlParameter;

/*
   Get a servo control parameter from the request.

   Handle the different accepted forms, picking the value from
   the request data with the same name:
   - action : semantic action (opàen, clopse, lock, unlock...)
   - position : servo position (in the same unit as the position range provided 
                in the configuration)
   - pulse : the pulse width, in micro-seconds
   - percent : the percentage (0-100) of the servo full travel
   - pos : same as "position"
   - pct : same as "percent"

    Parameters:
    - parm : ControlParameter structure, updated by the function

    Returns true if the value could be obtained from the request, false otherwise.
*/
bool get_control_arg(ControlParameter &parm, bool accept_action) {
    String value = server->arg("action");
    if (value != "") {
        if (!accept_action) {
            return false;
        }
        parm.parm_type = CONTROL_ACTION;
        strncpy(parm.str_value, value.c_str(), 9);
        return true;
    }

    value = server->arg("position");
    if (value == "") value = server->arg("pos");
    if (value != "") {
        parm.parm_type = CONTROL_POSITION;
        parm.int_value = value.toInt();
        return true;
    }

    value = server->arg("pulse");
    if (value != "") {
        parm.parm_type = CONTROL_PULSE;
        parm.int_value = value.toInt();
        return true;
    }

    value = server->arg("percent");
    if (value == "") value = server->arg("pct");
    if (value != "") {
        parm.parm_type = CONTROL_PERCENT;
        parm.int_value = value.toInt();
        return true;
    }

    return false;
}

/*
   Sends a HTTP response containing the door state in JSON format.
*/
void send_room_door_state(int room_num) {
    int room_ndx = room_num - 1;

    int pos = servo_states[configuration.rooms[room_ndx].door.servo_id];

    DeviceConfiguration *config_p = &configuration.rooms[room_ndx].door;
    const char *state;
    if (pos == config_p->door_pos.opened) {
        state = "opened";
    } else if (pos == config_p->door_pos.closed) {
        state = "closed";
    } else {
        state = "undef";
    }

    sprintf(printf_buffer, "{\"state\": \"%s\", \"position\": %d}", state, pos);
    server->send(200, "application/json", printf_buffer);
}

/*
   Sends a HTTP response containing the lid state in JSON format.
*/
void send_room_lid_state(int room_num) {
    int room_ndx = room_num - 1;

    int pos = servo_states[configuration.rooms[room_ndx].lid.servo_id];

    DeviceConfiguration *config_p = &configuration.rooms[room_ndx].lid;
    const char *state;
    if (pos == config_p->lock_pos.unlocked) {
        state = "unlocked";
    } else if (pos == config_p->lock_pos.locked) {
        state = "locked";
    } else {
        state = "undef";
    }

    sprintf(printf_buffer, "{\"state\": \"%s\", \"position\": %d}", state, pos);
    server->send(200, "application/json", printf_buffer);
}

/*
   Sends a HTTP response containing the servo position in JSON format.
*/
void send_servo_position(int pos) {
    sprintf(printf_buffer, "{\"position\": %d}", pos);
    server->send(200, "application/json", printf_buffer);
}

/*
   Sends a HTTP response containing the PWM enabling state in JSON format.

   Parameters :
   - is_enabled  : true if pulses are currently generated
*/
void send_pwm_state() {
    sprintf(printf_buffer, "{\"is_enabled\": %d}", pwm_enabled);
    server->send(200, "application/json", printf_buffer);
}

/*
   Sends a HTTP response containing a servo position in JSON format.

   Parameters :
   - pos  : the position
*/
void send_servo_pos(int pos) {
    sprintf(printf_buffer, "{\"position\": %d}", pos);
    server->send(200, "application/json", printf_buffer);
}

/*
   Sends a HTTP response containing a servo pulse width in JSON format.

   Parameters :
   - pulse  : the pulse width
*/
void send_servo_pulse(int pulse) {
    sprintf(printf_buffer, "{\"pulse\": %d}", pulse);
    server->send(200, "application/json", printf_buffer);
}

/*
   Sends a HTTP response containing a servo position in JSON format.

   Parameters :
   - percent  : the position, in percent of the total travel
*/
void send_servo_percent(int percent) {
    sprintf(printf_buffer, "{\"percent\": %d}", percent);
    server->send(200, "application/json", printf_buffer);
}

/*
    Sends an error response with a detail JSON structure
*/
void send_error(int status, const char *detail) {
    server->send(status, "application/json", "{\"detail\": \"" + String(detail) + "\"}");
}

/*
    Context holder for control requests
*/
   typedef struct {
    union {     // room or port is used depending on the command level (device of servo)
        int room;           
        int servo_id;
    };
    ControlParameter parm;
    DeviceConfiguration *device_config_p;       // set for device level context only
    ServoConfiguration *servo_config_p;
    bool success;
    int servo_resulting_position;
} ControlContext;

/*
   Internal helper for the shared pre-processing of sevices control requests.

   It takes care of extracting the room number from the path and the control argument
   from the request data. 

   It also selects and returns the configuration extract for the device passed as argument.

   Parameters :
   - device : the device name (currently "door" or "lid")
   - parm : a reference on the control parameter structure to update

   Returns a reference on the device specific configuration.
*/
static bool _device_control_preprocess(const char *device, ControlContext &context) {
    int room = get_room_arg();
    if (!room) return false;

    context.room = room;

    if (!get_control_arg(context.parm, true)) { 
        send_error(400, "missing or invalid control argument");
        return false;
    }

    RoomConfiguration *room_config_p = &configuration.rooms[room - 1];

    if (strcmp(device, "door") == 0) {
        context.device_config_p = &room_config_p->door; 
        context.servo_config_p = &configuration.servos[room_config_p->door.servo_id];
    } else if (strcmp(device, "lid") == 0) {
        context.device_config_p = &room_config_p->lid; 
        context.servo_config_p = &configuration.servos[room_config_p->lid.servo_id];
    } else {
        send_error(400, "missing or invalid device");
        return false;
    }

    return true;
}

/*
   Internal helper for the shared processing of servo control requests

   Parameters:
   - parm : the servo control parameters
   - config : the servo configuration
   - send_resp : if true, the completion HTTP response is sent directly with the new setting

   Returns true if the command could be executed.
*/
static bool _control_servo(int servo_id, ControlContext &context) {
    int pulse; 

    context.success = false;

    ServoConfiguration *servo_config_p =  context.servo_config_p;

    switch (context.parm.parm_type) {
        case CONTROL_POSITION: 
            if (context.parm.int_value < servo_config_p->pos_min 
                || context.parm.int_value > servo_config_p->pos_max) {
                send_error(400, "invalid position value");
                return false;
            }
            pulse = map(
                servo_config_p->inverted ? servo_config_p->pos_max - context.parm.int_value : context.parm.int_value, 
                servo_config_p->pos_min, servo_config_p->pos_max, 
                servo_config_p->pulse_min, servo_config_p->pulse_max
            );
            set_pwm(servo_id, pulse);
            context.servo_resulting_position = context.parm.int_value;
            break;

        case CONTROL_PULSE:
            if (context.parm.int_value < servo_config_p->pulse_min 
                || context.parm.int_value > servo_config_p->pulse_max) {
                send_error(400, "invalid pulse value");
                return false;
            }
            set_pwm(servo_id, context.parm.int_value);
            context.servo_resulting_position = map(
                context.parm.int_value,
                servo_config_p->pulse_min, servo_config_p->pulse_max,
                servo_config_p->pos_min, servo_config_p->pos_max 
            );
            break;

        case CONTROL_PERCENT:
            if (context.parm.int_value < 0 || context.parm.int_value > 100) {
                send_error(400, "invalid percent value");
                return false;
            }
            { 
                int pulse = map(context.parm.int_value, 
                        0, 100, 
                        servo_config_p->pulse_min, servo_config_p->pulse_max
                        ) ;
                set_pwm(servo_id, pulse);
            }
            context.servo_resulting_position = map(
                context.parm.int_value,
                0, 100,
                servo_config_p->pos_min, servo_config_p->pos_max 
            );
            break;
    } 

    context.success = true;
    return true;
}

static bool _servo_control_preprocess(ControlContext &context) {
    int servo_id = get_servo_id_arg();
    if (servo_id == -1) return false;

    context.servo_id = servo_id;
    context.servo_config_p = &configuration.servos[servo_id];


    if (!get_control_arg(context.parm, false)) { 
        send_error(400, "missing or invalid control argument");
        return false;
    }

    return true;
}

/*
   Request handlers
*/

/*
   Firmware version query
*/
void handle_version_query() {
    sprintf(printf_buffer, "{\"version\": \"%s\"}", VERSION);
    server->send(200, "application/json", printf_buffer);
}

/*
   Direct control of a servo
*/
void handle_servo_direct_control() {
    ControlContext context;
    if (!_servo_control_preprocess(context)) {
        return;     // response already sent
    }

    if (_control_servo(context.servo_id, context)) {
        int pos = servo_states[context.servo_id] = context.servo_resulting_position;
        send_servo_position(pos);
    }
}

/*
    Servo current state query
*/    
void handle_servo_current_state_query() {
    int servo_id = get_servo_id_arg();
    if (servo_id == -1) {
        return;
    }

    send_servo_position(servo_states[servo_id]);
}

/*
    Servo settings management
*/    

void _send_servo_settings(int servo_id) {
    String content;

    ServoConfiguration *config_p = &configuration.servos[servo_id];

    static char buffer[512]; 
    sprintf(buffer,
            "{"
            "\"pulse_min\": %d, \"pulse_max\": %d, \"pos_min\": %d, \"pos_max\": %d, \"inverted\": %d"
            "}",
            config_p->pulse_min, config_p->pulse_max, config_p->pos_min, config_p->pos_max, config_p->inverted
            );
    content += buffer;

    server->send(200, "application/json", content);
}

void handle_servo_settings_change() {
    int servo_id = get_servo_id_arg();
    if (servo_id == -1) return ;

    String name = server->arg("name");
    if (name == "") {
        send_error(400, "missing setting name");
        return;
    }

    char setting_fqn[100];
    sprintf(setting_fqn, "servos.%d.%s", servo_id, name.c_str());

    if (set_config_value(setting_fqn, server->arg("value").c_str())) {
        save_configuration(PUBLIC_SETTINGS);
        _send_servo_settings(servo_id);

    } else {
        send_error(400, "invalid setting name");
    }
}

void handle_servo_settings_query() {
    int servo_id = get_servo_id_arg();
    if (servo_id == -1) return ;

    _send_servo_settings(servo_id);
}

/*
   Door control 
*/   
void handle_door_control() {
    ControlContext context;
    if (!_device_control_preprocess("door", context)) {
        return;     // response already sent
    }

    // if a semantic action is use to control the servo, convert it
    // into the equivalent position control to fall into the generic process
    if (context.parm.parm_type == CONTROL_ACTION) {
        context.parm.parm_type = CONTROL_POSITION;
        if (strcmp(context.parm.str_value, "open") == 0) {
            context.parm.int_value = context.device_config_p->door_pos.opened; 
        } else if (strcmp(context.parm.str_value, "close") == 0) {
            context.parm.int_value = context.device_config_p->door_pos.closed;
        } else {
            send_error(400, "invalid action value");
            return;
        }
    }

    int servo_id = context.device_config_p->servo_id;
    if (_control_servo(servo_id, context)) {
        servo_states[servo_id] = context.servo_resulting_position;
        send_room_door_state(context.room);
    }
}

/*
   Door state query
*/   
void handle_door_state_query() {
    int room = get_room_arg();
    if (!room) return ;

    send_room_door_state(room);
}

/*
   Lid lock control
*/   
void handle_lid_control() {
    ControlContext context;
    if (!_device_control_preprocess("lid", context)) {
        return;     // response already sent
    }

    if (context.parm.parm_type == CONTROL_ACTION) {
        context.parm.parm_type = CONTROL_POSITION;
        if (strcmp(context.parm.str_value, "unlock") == 0) {
            context.parm.int_value = context.device_config_p->lock_pos.unlocked; 
        } else if (strcmp(context.parm.str_value, "lock") == 0) {
            context.parm.int_value = context.device_config_p->lock_pos.locked; 
        } else {
            send_error(400, "invalid action value");
            return;
        }
    }

    int servo_id = context.device_config_p->servo_id;
    if (_control_servo(servo_id, context)) {
        servo_states[servo_id] = context.servo_resulting_position;
        send_room_lid_state(context.room);
    }
}

/*
   Lid state query
*/   
void handle_lid_state_query() {
    int room = get_room_arg();
    if (!room) return ;

    send_room_lid_state(room);
}

/*
   PWM enbling control
*/   
void handle_pwm_control() {
    String action = get_action_arg();
    if (action == "") return;

    if (action == "enable") {
        set_pwm_state(PWM_ENABLED);    
    } else if (action == "disable") {
        set_pwm_state(PWM_DISABLED);
    }
    send_pwm_state();
}


/*
   PWM state query
*/   
void handle_pwm_state_query() {
    send_pwm_state();
}

/*
   Configuration query
*/
void handle_settings_query() {
    String format = server->arg("format");
    if (format == "raw") {
        File file = SPIFFS.open(PUBLIC_SETTINGS, "r");

        server->streamFile(file, "text/plain");
        file.close();

    } else {
        String content = "{";

        content += "\"servos\": {";

        for (int servo_id=0; servo_id < MAX_SERVOS; servo_id++) {
            ServoConfiguration *config_p = &configuration.servos[servo_id];

            static char buffer[512]; 
            sprintf(buffer,
                    "\"%d\": {"
                    "\"pulse_min\": %d, \"pulse_max\": %d, \"pos_min\": %d, \"pos_max\": %d"
                    "}",
                    servo_id,
                    config_p->pulse_min, config_p->pulse_max, config_p->pos_min, config_p->pos_max
                   );
            content += buffer;
            if (servo_id < MAX_SERVOS - 1) {
                content += ", ";
            }
        }

        content += "}, \"rooms\": {";

        for (int room_ndx=0; room_ndx < MAX_ROOMS; room_ndx++) {
            RoomConfiguration *config_p = &configuration.rooms[room_ndx];

            static char buffer[512]; 
            sprintf(buffer,
                    "\"%d\": {"
                    "\"door\": {"
                    "\"servo\": %d, \"pos_opened\": %d, \"pos_closed\": %d"
                    "},"
                    "\"lid\": {"
                    "\"servo\": %d, \"pos_unlocked\": %d, \"pos_locked\": %d"
                    "}}",
                    room_ndx + 1,

                    config_p->door.servo_id, config_p->door.door_pos.opened, config_p->door.door_pos.closed,
                    config_p->lid.servo_id, config_p->lid.lock_pos.unlocked, config_p->lid.lock_pos.locked
                   );
            content += buffer;
            if (room_ndx < MAX_ROOMS - 1) {
                content += ", ";
            }
        }

        content += "}}";

        server->send(200, "application/json", content);
    }
}

static void _send_device_settings(int room_num, const char *device) {
    int room_ndx = room_num - 1;

    DeviceConfiguration *config_p;
    
    static char buffer[512]; 

    if (strcmp(device, "door") == 0) {
        config_p = &configuration.rooms[room_ndx].door;
        sprintf(buffer,
                "{"
                "\"servo\": %d, \"pos_opened\": %d, \"pos_closed\": %d"
                "}",
                config_p->servo_id, config_p->door_pos.opened, config_p->door_pos.closed
                );

    } else if (strcmp(device, "lid") == 0) {
        config_p = &configuration.rooms[room_ndx].lid ;
        sprintf(buffer,
                "{"
                "\"servo\": %d, \"pos_unlocked\": %d, \"pos_locked\": %d"
                "}",
                config_p->servo_id, config_p->lock_pos.unlocked, config_p->lock_pos.locked
                );

    } else {
        send_error(500, "_send_device_settings: unexpected device value");
    }

    server->send(200, "application/json", String(buffer));
}

static bool _change_device_settings(int room_num, const char *device) {
    String name = server->arg("name");
    if (name == "") {
        send_error(400, "missing setting name");
        return false;
    }

    char setting_fqn[100];
    sprintf(setting_fqn, "rooms.%d.%s.%s", room_num, device, name.c_str());

    if (!set_config_value(setting_fqn, server->arg("value").c_str())) {
        send_error(400, "invalid setting name");
        return false;
    }

    save_configuration(PUBLIC_SETTINGS);
    _send_device_settings(room_num, device);
    return true;
}

static void _handle_device_settings_change(const char *device) {
    int room_num = get_room_arg();
    if (!room_num) return ;

    if (_change_device_settings(room_num, device)) {
        _send_device_settings(room_num, device);
    }
}

static void _handle_device_settings_query(const char *device) {
    int room_num = get_room_arg();
    if (!room_num) return ;

    _send_device_settings(room_num, device);
}

/*
   Door settings management
*/   
void handle_door_settings_change() {
    _handle_device_settings_change("door");
}

void handle_door_settings_query() {
    _handle_device_settings_query("door");
}

/*
   Lid settings management
*/   
void handle_lid_settings_change() {
    _handle_device_settings_change("lid");
}

void handle_lid_settings_query() {
    _handle_device_settings_query("lid");
}

/*
   Controller id query
*/   
void handle_controller_id_query() {
    sprintf(printf_buffer, "{\"controller_id\": \"%s\"}", controller_id.c_str());
    server->send(200, "application/json", printf_buffer);
}

/*
   Controller id change
*/   
void handle_controller_id_change() {
    String value = server->arg("value");
    if (value == "") {
        send_error(400, "missing value parameter");
    }

    controller_id = value;
    save_configuration(PUBLIC_SETTINGS);
    handle_controller_id_query();
}

/*
   ESP32 reboot request
*/
void handle_restart() {
    server->send(204, "text/plain", "");
    // wait a bit to be sure the response is fully sent before trigering the reboot
    delay(500);

    ESP.restart();
}
