#ifndef __handlers_h__
#define __handlers_h__

#include <WebServer.h>
#include <Adafruit_PWMServoDriver.h>

extern Adafruit_PWMServoDriver pwm ;
extern WebServer *server;

int get_room_arg() ;

void handle_version_query() ;

void handle_door_control() ;
void handle_door_state_query() ;
void handle_door_settings_change();
void handle_door_settings_query();

void handle_lid_control() ;
void handle_lid_state_query() ;
void handle_lid_settings_change() ;
void handle_lid_settings_query();

void handle_servo_direct_control();
void handle_servo_current_state_query();
void handle_servo_settings_change();
void handle_servo_settings_query();

void handle_pwm_control() ;
void handle_pwm_state_query() ;

void handle_settings_query() ;

void handle_controller_id_query();
void handle_controller_id_change();

void handle_restart() ;

#endif
