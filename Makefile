PROJECT_FILE=roomctrl.ino

ARDUINO_PORT?=/dev/ttyUSB0
ARDUINO_MONITOR_SPEED?=115200

default: upload

verify:
	arduino --verify $(PROJECT_FILE)

upload:
	arduino --upload --port $(ARDUINO_PORT) $(PROJECT_FILE)

monitor:
	screen $(ARDUINO_PORT) $(ARDUINO_MONITOR_SPEED)
	
