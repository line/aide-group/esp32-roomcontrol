/*
   RESP API server main
*/   

#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <SPIFFS.h>
#include "time.h"

#include "configuration.h"
#include "handlers.h"
#include "printf.h"

#define CONNECTION_TIMEOUT_SECS     5

void setup() {
    Serial.begin(115200);
    delay(10);

    if(!SPIFFS.begin(true)){
        Serial.println("An error has occurred while mounting SPIFFS");
        return;
    }

    // configuration and secrets loading

    load_configuration(PUBLIC_SETTINGS);
    load_configuration(SECRETS);

    print_configuration();

    // PCA9685 driver initialization

    Serial.println("initializing PCA board");
    pwm.begin();

    // The div is a workaround for the equivalent ratio applied to the
    // requested freq in the lib (cf Adafruit_PWMServoDriver.cpp:127).
    // This results in fact in a frequency // lower than the requested one 
    // and wrong pulse timings at the end of the day.
    // The exact value of the compensation ratio has been determined 
    // by trial and error to obtain the frequency closest to the expected one.

    // WARNING: because of HW tolerances, the default compensation factor value
    // may not work very well with your board. Modify the value in the
    // configuration file of the sketch data.
    pwm.setPWMFreq(PWM_FREQ / pwm_freq_compensation_dividor); 

    for (int port = 0 ; port < 8; port++) {
        pwm.setPWM(port, 0, 0);
    }

    pwm.sleep();

    // the onboard LED will be used to show Wifi connection status

    pinMode(13, OUTPUT);

    // connection to the Wifi network

    printfln("connecting to Wifi network %s...", ssid.c_str());

    WiFi.begin(ssid.c_str(), password.c_str());

    unsigned long time_limit = millis() + CONNECTION_TIMEOUT_SECS * 1000;

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        // make the LED blink while waiting for the connection
        digitalWrite(13, !digitalRead(13));

        if (millis() > time_limit) {
            // sometimes the connection does not succeed after first reboot => restart the board 
            // (it happens always when uploading the sketch from the IDE)
            Serial.println("Connection timeout. Restarting the board...");
            ESP.restart();
        }
    }

    printfln("connected with IP %s", WiFi.localIP().toString().c_str());

    // configure the mDNS advertisement. The ESP will be accessible with
    // hostname : roomctrl-<id>.local
    String hostname = String("roomctrl-") + controller_id;
    if (MDNS.begin(hostname.c_str())) {
        printfln("MDNS responder started with hostname '%s'", hostname.c_str());
        MDNS.addService("http", "_tcp", 8000);
    }

    // initialize the server and configure its URLs routing

    Serial.println("initializing Web server");
    server = new WebServer(server_listen_port);

    // ---------------------------------------------------------------------------------
    //  Routing table
    // ---------------------------------------------------------------------------------

    // high level API (aka "semantic")

    server->on("/rooms/{}/door", HTTP_POST, handle_door_control);
    server->on("/rooms/{}/door", HTTP_GET, handle_door_state_query);
    server->on("/rooms/{}/door/settings", HTTP_PUT, handle_door_settings_change);
    server->on("/rooms/{}/door/settings", HTTP_GET, handle_door_settings_query);

    server->on("/rooms/{}/lid", HTTP_POST, handle_lid_control);
    server->on("/rooms/{}/lid", HTTP_GET, handle_lid_state_query);
    server->on("/rooms/{}/lid/settings", HTTP_PUT, handle_lid_settings_change);
    server->on("/rooms/{}/lid/settings", HTTP_GET, handle_lid_settings_query);

    // low level API (servos direct control)

    server->on("/servos/{}", HTTP_POST, handle_servo_direct_control);
    server->on("/servos/{}", HTTP_GET, handle_servo_current_state_query);
    server->on("/servos/{}/settings", HTTP_PUT, handle_servo_settings_change);
    server->on("/servos/{}/settings", HTTP_GET, handle_servo_settings_query);

    // system and application level settings

    server->on("/pwm", HTTP_POST, handle_pwm_control);
    server->on("/pwm", HTTP_GET, handle_pwm_state_query);

    server->on("/settings", HTTP_GET, handle_settings_query);

    server->on("/id", HTTP_GET, handle_controller_id_query);
    server->on("/id", HTTP_PUT, handle_controller_id_change);

    // misc

    server->on("/restart", HTTP_POST, handle_restart);
    server->on("/", HTTP_GET, handle_version_query);   

    // ---------------------------------------------------------------------------------

    server->begin();
    printfln("HTTP server started and listening on port %d", server_listen_port);

    // turn the LED steady on to indicate we are up and running

    digitalWrite(13, 1);
}

void loop() {
    server->handleClient();
}
