#ifndef __printf_h__
#define __printf_h__

extern char printf_buffer[];

#define printf(fmt, ...) sprintf(printf_buffer, fmt, __VA_ARGS__) ; Serial.print(printf_buffer) ;
#define printfln(fmt, ...) sprintf(printf_buffer, fmt, __VA_ARGS__) ; Serial.println(printf_buffer) ;

#endif
