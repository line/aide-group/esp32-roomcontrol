#ifndef __configuration_h__
#define __configuration_h__
/*
    Configuration definition and management

    The configuration data are persisted in a properties-like file 
    stored in the SPIFFS.

    Keys are qualified names (i.e. the can be made of several parts joined by dots).
*/

// Servo level configuration
typedef struct {
    int pulse_min;
    int pulse_max;
    int pos_min;
    int pos_max;
    bool inverted;
} ServoConfiguration;

/*
    Predefined servo positions for doors
*/
typedef struct {
    int opened;
    int closed;
} DoorPositions;

/*
    Predefined servo positions for lid locks
*/
typedef struct {
    int locked;
    int unlocked;
} LockPositions;

/*
    Configuration of room devices (i.e. door, lock...)
*/    
typedef struct {
    // the id of the servo used by the device
    int servo_id;
    // the  device pre-defined positions
    union {
        DoorPositions door_pos;
        LockPositions lock_pos;
    };
} DeviceConfiguration;

/*
    Configuration of devices installed in a room
*/

typedef struct {
    DeviceConfiguration door;
    DeviceConfiguration lid;
} RoomConfiguration;

// --------------------------------------------------------------------------------
//  The global configuration
// --------------------------------------------------------------------------------

#define MAX_SERVOS  8
#define MAX_ROOMS   2

typedef struct {
    // servo level : the index in the array is the port number the servo is connected to
    ServoConfiguration servos[MAX_SERVOS];
    // room level : the index in the array is the room (1-based) number minus 1
    RoomConfiguration rooms[MAX_ROOMS];
} Configuration ;

extern Configuration configuration;

#define PUBLIC_SETTINGS "/roomctrl.conf"
#define SECRETS         "/secrets.conf"

#define PWM_FREQ    50

// --------------------------------------------------------------------------------
// The system level configuration parameters
// --------------------------------------------------------------------------------

// Wifi connection
extern String ssid;
extern String password;

// Controller id, used to build the hostname advertised on mDNS
extern String controller_id;

// HTTP server listening port
extern int server_listen_port;

// Compensation of the setting shift of the PCA PWM frequency
extern float pwm_freq_compensation_dividor;

// --------------------------------------------------------------------------------
// Public functions
// --------------------------------------------------------------------------------

extern void load_configuration(const char *file_path);
extern void save_configuration(const char *file_path);
extern void print_configuration();

extern bool set_config_value(const char *key, const char *value) ;

#endif
