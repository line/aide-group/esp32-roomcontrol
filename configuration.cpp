/*
   Configuration persistence management
*/

#include <SPIFFS.h>
#include "configuration.h"
#include "printf.h"

int server_listen_port = 80;

float pwm_freq_compensation_dividor = 0.943;

String controller_id = "0";

String ssid;
String password;

Configuration configuration;

/*
   Load the configuration from a SPIFFS file
*/   
void load_configuration(const char *file_path) {
    if (!SPIFFS.exists(file_path)) {
        printfln("ERROR: file not found : %s", file_path);
        return;
    }

    printfln("loading configuration from %s...", file_path);

    char key_buffer[40];
    char value_buffer[40];
    char *buf_p = key_buffer;

#define PARSER_IDLE         0
#define PARSER_IN_NAME      1
#define PARSER_IN_VALUE     2
#define PARSER_IN_COMMENTS  3

    int parser_state = PARSER_IDLE;

    File file = SPIFFS.open(file_path);
    while(file.available()) {
        char c = file.read();
        if (c == 0xff) continue;

        switch (parser_state) {
            case PARSER_IDLE:
                buf_p = key_buffer;
                if (c != ' ' && c != '\t' && c != '\r' && c != '\n') {
                    parser_state = PARSER_IN_NAME;
                    // no break here since we want to analyse the character we got
                } else if (c == '#') {
                    parser_state = PARSER_IN_COMMENTS;
                    break;
                } else {
                    break;
                }

            case PARSER_IN_NAME:
                switch (c) {
                    // ignore white spaces
                    case ' ':
                    case '\r':
                    case '\t':
                        break;
                        // if at the end of the line, it means that this is a blank one
                    case '\n':
                        buf_p = key_buffer;
                        parser_state = PARSER_IDLE;
                        break;
                        // at the end of the key name
                    case '=':
                        *buf_p = 0;
                        buf_p = value_buffer;
                        parser_state = PARSER_IN_VALUE;
                        break;
                        // getting chars of the key name
                    default:
                        *buf_p++ = c;
                        break;
                }
                break;

            case PARSER_IN_VALUE:
                switch (c) {
                    case '\n':
                        *buf_p = 0;
                        set_config_value(key_buffer, value_buffer);
                        parser_state = PARSER_IDLE;
                        break;

                    case ' ':
                    case '\r':
                    case '\t':
                        break;

                    default:
                        *buf_p++ = c;
                        break;
                }
                break;

            case PARSER_IN_COMMENTS:
                if (c == '\n') {
                    parser_state = PARSER_IDLE;
                }
                break;
        };
    }
    file.close();
    Serial.println("loading complete");
}

/*
   Save  the configuration to a SPIFFS file
*/   

void save_configuration(const char *file_path) {
    File file = SPIFFS.open(file_path, "w");

    sprintf(printf_buffer, "controller_id=%s\n", controller_id.c_str());
    file.write((const unsigned char *)printf_buffer, strlen(printf_buffer));

    sprintf(printf_buffer, "server.port=%d\n", server_listen_port);
    file.write((const unsigned char *)printf_buffer, strlen(printf_buffer));
    
    sprintf(printf_buffer, "pwm_freq_compensation_dividor=%f\n", pwm_freq_compensation_dividor);
    file.write((const unsigned char *)printf_buffer, strlen(printf_buffer));
    
    for (int servo_id = 0; servo_id < MAX_SERVOS; servo_id++) {
        ServoConfiguration *config_p = &configuration.servos[servo_id];

        sprintf(printf_buffer, "servos.%d.pos_min=%d\n", servo_id, config_p->pos_min);
        file.write((const unsigned char *)printf_buffer, strlen(printf_buffer));

        sprintf(printf_buffer, "servos.%d.pos_max=%d\n", servo_id, config_p->pos_max);
        file.write((const unsigned char *)printf_buffer, strlen(printf_buffer));

        sprintf(printf_buffer, "servos.%d.pulse_min=%d\n", servo_id, config_p->pulse_min);
        file.write((const unsigned char *)printf_buffer, strlen(printf_buffer));

        sprintf(printf_buffer, "servos.%d.pulse_max=%d\n", servo_id, config_p->pulse_max);
        file.write((const unsigned char *)printf_buffer, strlen(printf_buffer));

        sprintf(printf_buffer, "servos.%d.inverted=%d\n", servo_id, config_p->inverted);
        file.write((const unsigned char *)printf_buffer, strlen(printf_buffer));
    }

    for (int room_ndx = 0; room_ndx < MAX_ROOMS; room_ndx++) {
        int room_num = room_ndx + 1;
        RoomConfiguration *config_p = &configuration.rooms[room_ndx];

        // door settings

        sprintf(printf_buffer, "rooms.%d.door.servo=%d\n", room_num, config_p->door.servo_id);
        file.write((const unsigned char *)printf_buffer, strlen(printf_buffer));

        sprintf(printf_buffer, "rooms.%d.door.pos_opened=%d\n", room_num, config_p->door.door_pos.opened);
        file.write((const unsigned char *)printf_buffer, strlen(printf_buffer));

        sprintf(printf_buffer, "rooms.%d.door.pos_closed=%d\n", room_num, config_p->door.door_pos.closed);
        file.write((const unsigned char *)printf_buffer, strlen(printf_buffer));

        // lid settings

        sprintf(printf_buffer, "rooms.%d.lid.servo=%d\n", room_num, config_p->lid.servo_id);
        file.write((const unsigned char *)printf_buffer, strlen(printf_buffer));

        sprintf(printf_buffer, "rooms.%d.lid.pos_unlocked=%d\n", room_num, config_p->lid.lock_pos.unlocked);
        file.write((const unsigned char *)printf_buffer, strlen(printf_buffer));

        sprintf(printf_buffer, "rooms.%d.lid.pos_locked=%d\n", room_num, config_p->lid.lock_pos.locked);
        file.write((const unsigned char *)printf_buffer, strlen(printf_buffer));
    }

    file.close();
}

/*
   Print the configuration on the serial console
*/   
void print_configuration() {
    printfln("- controller_id = %s", controller_id.c_str());
    printfln("- server.port = %d", server_listen_port);
    printfln("- pwm_freq_compensation_dividor = %f", pwm_freq_compensation_dividor);
    printfln("- wifi.ssid = %s", ssid.c_str());
    Serial.println("- wifi.password = ********");

    for (int servo_id = 0; servo_id < MAX_SERVOS; servo_id++) {
        ServoConfiguration *config_p = &configuration.servos[servo_id];
        printfln(
                "- servo %d: pulse_min=%d pulse_max=%d pos_min=%d pos_max=%d inverted=%d",
                servo_id,
                config_p->pulse_min, config_p->pulse_max,
                config_p->pos_min, config_p->pos_max,
                config_p->inverted
              );
    }

    for (int room_ndx = 0; room_ndx < MAX_ROOMS ; room_ndx++) {
        RoomConfiguration *config_p = &configuration.rooms[room_ndx];
        int room_num = room_ndx + 1;
        
        printfln(
                "- room %d: door=(servo=%d opened=%d closed=%d)",
                room_num, config_p->door.servo_id, 
                config_p->door.door_pos.opened, config_p->door.door_pos.closed
              );
        printfln(
                "- room %d: lid=(servo=%d unlocked=%d locked=%d)",
                room_num, config_p->lid.servo_id, 
                config_p->lid.lock_pos.unlocked, config_p->lid.lock_pos.locked
               );
    }
}

static bool _set_servo_config_value(int servo_id, const char *key, const char *value) {
    ServoConfiguration *config_p = &configuration.servos[servo_id];

    if (strcmp(key, "pulse_min") == 0) {
        config_p->pulse_min = atoi(value);
    } else if (strcmp(key, "pulse_max") == 0) {
        config_p->pulse_max = atoi(value);
    } else if (strcmp(key, "pos_max") == 0) {
        config_p->pos_max = atoi(value);
    } else if (strcmp(key, "pos_min") == 0) {
        config_p->pos_min = atoi(value);
    } else if (strcmp(key, "inverted") == 0) {
        config_p->inverted = value[0] != '0';        // anything not 0 is considered as true

    } else {
        return false;
    }

    return true;
}

static bool _set_room_config_value(int room_ndx, const char *key, const char *value) {
    const char *device_type = key;
    char *p = (char *)strchr(device_type, '.');
    *p = 0;

    char *setting = p + 1;

    DeviceConfiguration *config_p;

    if (strcmp(device_type, "door") == 0) {
        config_p = &configuration.rooms[room_ndx].door;
        
        if (strcmp(setting, "servo") == 0) {
            config_p->servo_id = atoi(value);
            return true;
        }
        if (strcmp(setting, "pos_opened") == 0) {
            config_p->door_pos.opened = atoi(value);
            return true;
        }
        if (strcmp(setting, "pos_closed") == 0) {
            config_p->door_pos.closed = atoi(value);
            return true;
        }
    } 

    if (strcmp(device_type, "lid") == 0) {
        config_p = &configuration.rooms[room_ndx].lid;

        if (strcmp(setting, "servo") == 0) {
            config_p->servo_id = atoi(value);
            return true;
        }
        if (strcmp(setting, "pos_locked") == 0) {
            config_p->lock_pos.locked = atoi(value);
            return true;
        }
        if (strcmp(setting, "pos_unlocked") == 0) {
            config_p->lock_pos.unlocked = atoi(value);
            return true;
        }
    }

    return false;
}

/*
   Set a configuration parameter
*/
bool set_config_value(const char *name, const char *value) {
    if (strcmp(name, "server.port") == 0) {
        server_listen_port = atoi(value);
        return true;
    }

    if (strcmp(name, "controller_id") == 0) {
        controller_id = String(value);
        return true;
    }

    if (strcmp(name, "wifi.ssid") == 0) {
        ssid = String(value);
        return true;
    }

    if (strcmp(name, "wifi.password") == 0) {
        password = String(value);
        return true;
    }

    if (strcmp(name, "pwm_freq_compensation_dividor") == 0) {
        pwm_freq_compensation_dividor = atof(value);
        return true;
    }

    if (strncmp(name, "servos.", 7) == 0) {
        int servo_id = name[7] - '0';    // get the room number and convert it to index
        name += 9;                       // move past the servo id part
        return _set_servo_config_value(servo_id, name, value);
    }

    if (strncmp(name, "rooms.", 6) == 0) {
        int room_ndx = name[6] - '1';    // get the room number and convert it to index
        name += 8;                       // move past the room number part
        return _set_room_config_value(room_ndx, name, value);
    }

    return false;
}
